# Urban SDK Interview Homework

Link to Site: https://brown-avery-urban-hw.netlify.com/

This was the amount of work that I been able to accomplish in the span of 10 hours.

* Create navigation bar
* Have dashboard section that loads the following details about the latest accident (default) or a selected accident which is done by clicking a car icon on the Mapbox
  - Date of accident
  - Time of accident
  - Collision Id of accident
  - Borough
  - Street
  - 1st vehicle involved
  - 1st vehicle's role in accident
  - 2nd vehicle involved
  - 2nd vehicle's role in accident
  - Injuries
  - Fatalities
* Custom Mapbox intergration