import React from "react";
import MapInfo from "./layout/mapInfo";
import Header from "./layout/header";
import SubHeader from "./layout/subheader";

import ResultContextProvider from "./services/context/ResultContext";
import SelectedIncidentContextProvider from "./services/context/SelectedIncidentContext";

import { theme } from "./styles/theme";
import { makeStyles } from "@material-ui/core/styles";
import { Container, ThemeProvider } from "@material-ui/core";

export default function App() {
  const useStyles = makeStyles({
    container: {
      height: "100vh",
      overflowY: "scroll"
    }
  });
  const classes = useStyles();
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <ResultContextProvider>
          <SelectedIncidentContextProvider>
            <Header />
            <Container fixed className={classes.container}>
              <SubHeader />
              <MapInfo />
            </Container>
          </SelectedIncidentContextProvider>
        </ResultContextProvider>
      </ThemeProvider>
    </div>
  );
}
