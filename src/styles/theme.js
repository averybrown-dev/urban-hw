import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#273142"
    },
    secondary: {
      main: "#1b2431"
    },
    typography: {
      fontFamily: "Poppins"
    },
    overrides: {
      MuiCssBaseline: {
        "@body": {
          "@font-face": "Poppins"
        }
      }
    }
  }
});
