import { incident } from "../incident.js";

//grabs values of most recent 1000 traffic incidents and shortens to 100
export const fetchIncidentData = async () => {
  const results = await incident.get();
  const firstIncidents = results.data.value;
  return firstIncidents.slice(0, 100);
};

//filters out incidents without locations for mapbox
export const fetchIncidentMarkers = async () => {
  const incidents = await fetchIncidentData();
  return incidents
    .filter(incident => incident.location !== null)
    .map(incident => incident);
};

//takes floating time stamp and converts it to the correct string format
const formatDate = async date => {
  const month = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  const extracted_date = date.split("T")[0];
  const split_date = extracted_date.split("-");
  const month_num = parseInt(split_date[1]);
  //Returns date in a "Month, Day Year" format
  return `${month[month_num - 1]} ${split_date[2]}, ${split_date[0]}`;
};

//converts military time to standard time
const formatTime = async time => {
  let hour = parseInt(time.split(":")[0], 10);
  const min = time.split(":")[1];

  if (hour === 0) return `12: ${min} AM`;
  if (hour < 12) return `${hour} ${min} AM`;
  if (hour === 12) return `${hour} ${min} PM`;
  else return `${hour - 12} ${min} PM`;
};

//fetchs data specifically concerning the latest recorded traffic incident
export const fetchRecentIncident = async recent => {
  if (!recent) {
    const incidents = await fetchIncidentData();
    recent = incidents[0];
  }

  //location data
  const street = recent.on_street_name ? recent.on_street_name : "Unspecified";
  const borough = recent.borough ? recent.borough : "Unspecified";

  //injury and death count
  const injured = Number.isInteger(recent.number_of_persons_injured)
    ? recent.number_of_persons_injured
    : "N/A";
  const killed = Number.isInteger(recent.number_of_persons_killed)
    ? recent.number_of_persons_killed
    : "N/A";

  //identification of incident
  const collision_id = recent.collision_id ? recent.collision_id : "N/A";

  //vehicles role in collision
  const vehicle_1_contribution = recent.contributing_factor_vehicle_1
    ? recent.contributing_factor_vehicle_1
    : "N/A";
  const vehicle_2_contribution = recent.contributing_factor_vehicle_2
    ? recent.contributing_factor_vehicle_2
    : "N/A";
  const vehicle_3_contribution = recent.contributing_factor_vehicle_3
    ? recent.contributing_factor_vehicle_3
    : "N/A";
  const vehicle_4_contribution = recent.contributing_factor_vehicle_4
    ? recent.contributing_factor_vehicle_4
    : "N/A";
  const vehicle_5_contribution = recent.contributing_factor_vehicle_5
    ? recent.contributing_factor_vehicle_5
    : "N/A";

  //vehicles involved in collision
  const vehicle_1_code = recent.vehicle_type_code1
    ? recent.vehicle_type_code1
    : "N/A";
  const vehicle_2_code = recent.vehicle_type_code2
    ? recent.vehicle_type_code2
    : "N/A";
  const vehicle_3_code = recent.vehicle_type_code3
    ? recent.vehicle_type_code3
    : "N/A";
  const vehicle_4_code = recent.vehicle_type_code4
    ? recent.vehicle_type_code4
    : "N/A";
  const vehicle_5_code = recent.vehicle_type_code5
    ? recent.vehicle_type_code5
    : "N/A";

  //has to be reformatted for application
  const timestamped_date = recent.crash_date ? recent.crash_date : "N/A";
  const military_time = recent.crash_time ? recent.crash_time : "N/A";

  //if recorded, converts date from timestampped version to appropriate format
  const date =
    timestamped_date !== "N/A"
      ? await formatDate(timestamped_date)
      : timestamped_date;
  //returns military time format as standard time
  const standard_time =
    military_time !== "N/A" ? await formatTime(military_time) : military_time;

  return [
    {
      street,
      borough,
      injured,
      killed,
      collision_id,
      vehicle_1_code,
      vehicle_2_code,
      vehicle_3_code,
      vehicle_4_code,
      vehicle_5_code,
      vehicle_1_contribution,
      vehicle_2_contribution,
      vehicle_3_contribution,
      vehicle_4_contribution,
      vehicle_5_contribution,
      date,
      standard_time
    }
  ];
};

//separates loaded data by borough
export const filterByBorough = async () => {
  const incidents = await fetchIncidentData();

  const bronx = incidents.filter(incident => incident.borough === "BRONX");

  const brooklyn = incidents.filter(
    incident => incident.borough === "BROOKLYN"
  );
  const manhattan = incidents.filter(
    incident => incident.borough === "MANHATTAN"
  );
  const queens = incidents.filter(incident => incident.borough === "QUEENS");
  const staten_island = incidents.filter(
    incident => incident.borough === "STATEN_ISLAND"
  );
  const not_listed = incidents.filter(incident => incident.borough === null);

  return [
    bronx.length,
    brooklyn.length,
    manhattan.length,
    queens.length,
    staten_island.length,
    not_listed.length
  ];
};
