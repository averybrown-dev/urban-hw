import React, { createContext, useEffect, useState } from "react";
import { fetchRecentIncident } from "../api/fetch/fetchInfo";

export const SelectedIncidentContext = createContext();

const SelectedIncidentContextProvider = props => {
  const [selectedIncident, setSelectedIncident] = useState([
    {
      street: "",
      borough: "",
      injured: 0,
      killed: 0,
      collision_id: 0,
      vehicle_1_code: "",
      vehicle_2_code: "",
      vehicle_3_code: "",
      vehicle_4_code: "",
      vehicle_5_code: "",
      vehicle_1_contribution: "",
      vehicle_2_contribution: "",
      vehicle_3_contribution: "",
      vehicle_4_contribution: "",
      vehicle_5_contribution: "",
      date: "",
      standard_time: ""
    }
  ]);

  useEffect(() => {
    (async function() {
      setSelectedIncident(await fetchRecentIncident());
    })();
  }, []);
  return (
    <SelectedIncidentContext.Provider
      value={{ selectedIncident, setSelectedIncident }}
    >
      {props.children}
    </SelectedIncidentContext.Provider>
  );
};

export default SelectedIncidentContextProvider;
