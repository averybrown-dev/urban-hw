import React, { createContext, useEffect, useState } from "react";
import { fetchIncidentMarkers } from "../api/fetch/fetchInfo";

export const ResultContext = createContext();

const ResultContextProvider = props => {
  const [results, setResults] = useState([]);

  useEffect(() => {
    (async function() {
      setResults(await fetchIncidentMarkers());
    })();
  }, []);

  return (
    <ResultContext.Provider value={{ results, setResults }}>
      {props.children}
    </ResultContext.Provider>
  );
};

export default ResultContextProvider;
