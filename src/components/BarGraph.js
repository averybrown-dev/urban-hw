import React, { useEffect, useMemo, useRef } from "react";
import { Chart } from "chart.js";

export default function BarGraph() {
  const ref = useRef();

  const data = useMemo(() => ({
    labels: [
      "Bronx",
      "Brooklen",
      "Manhattan",
      "Queens",
      "Staten Island",
      "Other"
    ]
  }));

  useEffect(() => {
    const chart = new Chart(ref.current, {
      data,
      type: "bar",
      options: {
        title: { display: false },
        tooltips: {
          intersect: false,
          mode: "nearest",
          xPadding: 10,
          yPadding: 10,
          caretPadding: 10
        },
        legend: { display: false },
        responsive: true,
        maintainAspectRatio: false,
        barRadius: 4,
        scales: {
          xAxes: [{ display: false, gridLines: false, stacked: true }],
          yAxes: [{ display: false, stacked: true, gridLines: false }]
        },
        layout: { padding: { left: 0, right: 0, top: 0, bottom: 0 } }
      }
    });

    return () => {
      chart.destroy();
    };
  }, data);

  return <></>;
}
