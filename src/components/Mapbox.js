import React, { useContext, useState } from "react";
import ReactMapGl, { Marker } from "react-map-gl";

import { ResultContext } from "../services/context/ResultContext";
import { SelectedIncidentContext } from "../services/context/SelectedIncidentContext";

import { makeStyles } from "@material-ui/core/styles";
import { Button, Card, CardContent, Container } from "@material-ui/core";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCarCrash } from "@fortawesome/free-solid-svg-icons";

import "../styles/mapbox.scss";
import { fetchRecentIncident } from "../services/api/fetch/fetchInfo";

const Mapbox = () => {
  const [viewport, setViewport] = useState({
    latitude: 40.7128,
    longitude: -74.006,
    width: "98%",
    height: "26em",
    zoom: 12
  });
  const { results } = useContext(ResultContext);
  const { setSelectedIncident } = useContext(SelectedIncidentContext);

  const useStyles = makeStyles({
    button: {
      backgroundColor: "#32aac7",
      height: "100%"
    },
    card: {
      boxShadow: "none",
      width: "100%",
      height: "32em",
      paddingBottom: "2em"
    }
  });

  const classes = useStyles();

  //enables change of information based on selected crash
  const changeLocation = async incident => {
    setSelectedIncident(await fetchRecentIncident(incident));
  };

  return (
    <Card className={classes.card}>
      <CardContent>
        <div className="mapbox-header_container">
          <h2 className="mapbox-header"> City of New York</h2>
          <h4 className="mapbox-header_description">
            {" "}
            Most Recent Incidents In The Last Hour
          </h4>
        </div>
        <Container fixed>
          <ReactMapGl
            {...viewport}
            mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
            mapStyle="mapbox://styles/averybrown-dev/ck5bj2cf825t41cunn0hhq5lo"
            onViewportChange={viewport => {
              setViewport(viewport);
            }}
          >
            {results.map(incident => (
              <Marker
                latitude={incident.location.coordinates[1]}
                longitude={incident.location.coordinates[0]}
              >
                <Button
                  className={classes.button}
                  onClick={e => {
                    e.preventDefault();
                    changeLocation(incident);
                  }}
                >
                  <FontAwesomeIcon icon={faCarCrash} className="crash-icon" />
                </Button>
              </Marker>
            ))}
          </ReactMapGl>
        </Container>
      </CardContent>
    </Card>
  );
};

export default Mapbox;
