import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import { AppBar, Avatar, Box, Button, Toolbar } from "@material-ui/core";

import logo from "../../assets/logo-icon.svg";

import "../../styles/header.scss";

const useStyles = makeStyles(theme => ({
  active: {
    color: "white",
    backgroundColor: "#263040",
    paddingLeft: "2em",
    paddingRight: "2em"
  },
  button: {
    color: "white",
    paddingLeft: "2em",
    paddingRight: "2em"
  },
  layout: {
    boxShadow: "none",
    display: "flex",
    justifyContent: "space-between"
  },
  square: {
    backgroundColor: "#516173",
    borderRadius: "5px",
    marginLeft: "10px"
  },
  toolbar: {},
  user: {
    width: "auto",
    display: "flex",
    alignItems: "center",
    marginRight: theme.spacing(2)
  }
}));

export default function Header() {
  const classes = useStyles();
  return (
    <>
      <AppBar position="static" color="primary">
        <Toolbar className={classes.layout}>
          <img src={logo} alt="urban sdk logo" className="logo" />
          <Box className={classes.user}>
            <p className="user-greeting"> Hi, Sean</p>
            <Avatar variant="square" className={classes.square}>
              S
            </Avatar>
          </Box>
        </Toolbar>
      </AppBar>
      <AppBar position="static" color="secondary">
        <Toolbar>
          <Button className={classes.active}>Dashboards</Button>
          <Button className={classes.button}>Archives</Button>
          <Button className={classes.button}>Settings</Button>
        </Toolbar>
      </AppBar>
    </>
  );
}
