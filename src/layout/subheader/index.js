import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import { Breadcrumbs, Button, Link } from "@material-ui/core";

export default function SubHeader() {
  const useStyles = makeStyles({
    addButton: {
      backgroundColor: "#fafbfc",
      color: "#f7981c"
    },
    breadcrumb: {
      color: "#1b2431"
    },
    breadcrumbsContainer: {
      padding: ".75em"
    }
  });

  const classes = useStyles();

  return (
    <>
      <Breadcrumbs separator="|" className={classes.breadcrumbsContainer}>
        <Link className={classes.breadcrumb}>Dashboard</Link>
        <Button className={classes.addButton}>Add New</Button>
      </Breadcrumbs>
    </>
  );
}
