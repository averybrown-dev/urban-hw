import React, { useState } from "react";
import Mapbox from "../../components/Mapbox";

import { makeStyles } from "@material-ui/core/styles";
import { Container, Grid } from "@material-ui/core";

import Location from "./details/Location";
import Injured from "./details/Injured";
import Collision from "./details/Collision";
import Vehicles from "./details/Vehicles";

export default function MapInfo() {
  const useStyles = makeStyles({
    container: { display: "flex", flexDirection: "column", height: "100vh" },
    grid: {
      display: "flex",
      justifyContent: "space-between",
      marginBottom: "1em"
    }
  });

  const classes = useStyles();

  return (
    <Container fixed className={classes.container}>
      <Grid container>
        <Grid className={classes.grid} item xs container>
          <Collision />
          <Location />
        </Grid>
        <Grid className={classes.grid} container>
          <Vehicles />
          <Injured />
        </Grid>
      </Grid>
      <Mapbox />
    </Container>
  );
}
