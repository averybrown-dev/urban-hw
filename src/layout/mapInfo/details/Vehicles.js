import React, { useContext } from "react";
import { SelectedIncidentContext } from "../../../services/context/SelectedIncidentContext";

import { makeStyles } from "@material-ui/core/styles";
import { Card, CardContent } from "@material-ui/core";

export default function Vehicles() {
  const { selectedIncident } = useContext(SelectedIncidentContext);

  const useStyles = makeStyles({
    card: {
      boxShadow: "none",
      height: "12em",
      width: "40%",
      padding: "1.5em",
      overflow: "scroll"
    }
  });

  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardContent>
        <h2>Vehicles Involved</h2>
        <p>{selectedIncident[0].vehicle_1_code}</p>
        <p>Contribution: {selectedIncident[0].vehicle_1_contribution}</p>
        <p>{selectedIncident[0].vehicle_2_code}</p>
        <p>Contribution: {selectedIncident[0].vehicle_2_contribution}</p>
      </CardContent>
    </Card>
  );
}
