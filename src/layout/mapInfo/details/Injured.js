import React, { useContext } from "react";
import { SelectedIncidentContext } from "../../../services/context/SelectedIncidentContext";

import { makeStyles } from "@material-ui/core/styles";
import { Card, CardContent } from "@material-ui/core";

export default function Injured() {
  const { selectedIncident } = useContext(SelectedIncidentContext);

  const useStyles = makeStyles({
    card: {
      boxShadow: "none",
      height: "12em",
      width: "40%",
      padding: "1.5em"
    }
  });

  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardContent>
        <h2>Casualties</h2>
        <p>Injured: {selectedIncident[0].injured}</p>
        <p>Killed: {selectedIncident[0].killed}</p>
      </CardContent>
    </Card>
  );
}
