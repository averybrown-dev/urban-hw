import React, { useContext } from "react";
import { SelectedIncidentContext } from "../../../services/context/SelectedIncidentContext";

import { makeStyles } from "@material-ui/core/styles";
import { Card, CardContent } from "@material-ui/core";

export default function Collision() {
  const { selectedIncident } = useContext(SelectedIncidentContext);

  const useStyles = makeStyles({
    card: {
      boxShadow: "none",
      height: "12em",
      width: "40%",
      padding: "1.5em"
    }
  });

  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardContent>
        <h3>{selectedIncident[0].date}</h3>
        <p>Collision ID: {selectedIncident[0].collision_id}</p>
        <p>Time of Incident: {selectedIncident[0].standard_time}</p>
      </CardContent>
    </Card>
  );
}
