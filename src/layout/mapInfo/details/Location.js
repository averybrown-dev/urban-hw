import React, { useContext } from "react";
import { SelectedIncidentContext } from "../../../services/context/SelectedIncidentContext";

import { makeStyles } from "@material-ui/core/styles";
import { Card, CardContent } from "@material-ui/core";

export default function Location() {
  const { selectedIncident } = useContext(SelectedIncidentContext);

  const useStyles = makeStyles({
    card: {
      boxShadow: "none",
      height: "12em",
      width: "40%",
      padding: "1.5em"
    }
  });

  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardContent>
        <h2>Location</h2>
        <p>Borough: {selectedIncident[0].borough}</p>
        <p>Street: {selectedIncident[0].street}</p>
      </CardContent>
    </Card>
  );
}
